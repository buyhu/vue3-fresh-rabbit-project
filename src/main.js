import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 引入初始化的样式
import '@/styles/common.less'

// 想要在组件的style语法块中使用某个less文件中的变量！！！

// 没有用插件的时候 报错  @xtxColor is not defined  变量没有找到 -> 作用域的问题

// 原因：每一个style语法块内部都是一个独立的作用域(函数) 只有把变量导入之后 在当前的作用域内部才能访问到
// 而main.js导入 并不能实现作用域

// 注册一下共用组件
import componentPlugin from '@/components'
// 注册一下指令插件
import directivePlugin from '@/directives'

createApp(App).use(store).use(router).use(componentPlugin).use(directivePlugin).mount('#app')
