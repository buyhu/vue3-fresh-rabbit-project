import { createRouter, createWebHashHistory } from 'vue-router'
import Login from '@/views/Login'
import Layout from '@/views/Layout'
import Category from '@/views/Category'
import Home from '@/views/Home'
const routes = [
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    children: [
      {
        path: '',
        component: Home
      },
      {
        path: 'category/:id',
        component: Category
      }
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
