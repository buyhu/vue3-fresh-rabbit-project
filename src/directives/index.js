// 指令的语法

/*
app.direative('指令名称', {
  mounted(el,binding){
    el: 指令绑定到的元素
    binding.value: 指令等于号后面的表达式的数据
  }
})
*/

// 插件的定义
/**
  app.use()
  const plugin = {
    install(app){
      // app.component
      // app.directive
      // ...
    }
  }
 */

// 整体方向： 插件 -> 指令 -> 检测图片是否进入到视口区域如果进入就正式加载图片
// 核心逻辑： 1. 检测进入视口(检测滚动距离自行判断 / vueUse)   2.发起图片请求 img.src = url
// 实现步骤： 先编写一个没有逻辑逻辑插件  -> 编写一个没有逻辑的指令 -> 编写具体的插件逻辑
import { useIntersectionObserver } from '@vueuse/core'
const plugin = {
  install (app) {
    console.log(app)
    app.directive('lazy-img', {
      mounted (el, binding) {
        // 编写核心逻辑
        const { stop } = useIntersectionObserver(
          el,
          ([{ isIntersecting }], observerElement) => {
            // isIntersecting true：进入到了视口  false：在视口外
            console.log(isIntersecting)
            // 监听是一直存在的 每次切换视口 都会重复执行这里的回调
            // 期望：只要图片加载完一次之后 停止监听才对
            if (isIntersecting) {
              // 发送图片请求
              el.src = binding.value
              // 停止监听
              stop()
            }
          }
        )
      }
    })
  }
}

export default plugin
