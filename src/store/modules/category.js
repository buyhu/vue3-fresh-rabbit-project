// 导入接口
import { getCategory } from '@/api/home'
export default {
  namespaced: true,
  state: {
    categoryList: []
  },
  mutations: {
    setCategoryList (state, list) {
      state.categoryList = list
    }
  },
  actions: {
    // 调用接口  选一个合适的地方把这个action触发一下
    async asyncLoadList (ctx) {
      const res = await getCategory()
      ctx.commit('setCategoryList', res.result)
    }
  }
}
