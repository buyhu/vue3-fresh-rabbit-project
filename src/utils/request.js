// 封装axios

import axios from 'axios'

// 1. create实例化  baseURL + timeout

const instance = axios.create({
  baseURL: 'http://pcapi-xiaotuxian-front-devtest.itheima.net',
  timeout: 10000
})

// 2. 请求拦截器  注入token request header
instance.interceptors.request.use((config) => {
  // 在发送请求之前做些什么
  return config
}, (error) => {
  // 对请求错误做些什么
  return Promise.reject(error)
})

// 3. 响应拦截器 [1.2xx  2. token失效 401  3. 根据后端自定义状态码抛出正确返回和异常]
instance.interceptors.response.use((response) => {
  // 对响应数据做点什么
  return response.data
}, (error) => {
  // 对响应错误做点什么
  return Promise.reject(error)
})

// 需求描述
// 1. 封装一个函数 这个函数可以用来发送ajax请求
// 2. 这个函数的使用方式如下
//    http(url,method,data).then(res=>{console.log(res)})
// 3. 这个函数不管我传入的method是get/post 要求data都可以是一个普通的对象

// 用户传入的method  -> 'get' 'Get' 'geT' 程序健壮性

function http (url, method, data) {
  return instance({
    url,
    method,
    // 这里参数属性到底叫啥不确定  以method决定 如果它为get -> params post ->data
    // 不管用户传入的对不对 统一都处理成大写 然后再做判断
    [method.toUpperCase() === 'GET' ? 'params' : 'data']: data
  })
}

export default http
